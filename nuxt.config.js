import { dirname, join } from 'path'
import querystring from 'querystring'
import { readFile } from 'fs/promises'
import Image from '@11ty/eleventy-img'
import Cheerio from 'cheerio'
import { detectLanguage } from './lib/content/detectLanguage'

const SITE_URL = 'https://romaricpascal.is'
const FEED_DESCRIPTIONS = {
	en: 'Thoughts about front-end development (mostly)',
	fr: "Pensées sur l'intégration web (en gros)",
}
const CREATIVE_FEED_DESCRIPTIONS = {
	en: 'Steps of my creative journey',
	fr: 'Les pas de mon voyage creatif',
}

const LOCALES = {
	locales: [
		{
			code: 'en',
			iso: 'en',
			name: 'English',
		},
		{ code: 'fr', iso: 'fr', name: 'Français' },
	],
	defaultLocale: 'en',
}

const DATE_TIME_FORMATS = {
	'en-gb': {
		// Require 'en-gb' here so that date is in the right order
		short: {
			year: 'numeric',
			month: 'short',
			day: '2-digit',
		},
	},
	fr: {
		short: {
			year: 'numeric',
			month: 'short',
			day: '2-digit',
		},
	},
}

const EXTRA_JS = false // { main: ['./assets/main.js'] };

export default {
	// Target (https://go.nuxtjs.dev/config-target)
	target: 'static',

	// Global page headers (https://go.nuxtjs.dev/config-head)
	head: {
		titleTemplate: (titleChunk) => {
			if (titleChunk) {
				return `${titleChunk} | Romaric Pascal`
			}
			return 'Romaric Pascal'
		},
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: '' },

			// Social metadata
			{ property: 'og:type', content: 'website' },
			{ property: 'og:image', content: `${SITE_URL}/media/meta.png` },
			{ property: 'twitter:card', content: 'summary' },
			{ property: 'twitter:creator', content: 'romaricpascal' },

			// Favicon stuff
			{ name: 'msapplication-TileColor', content: '#111111' },
			{
				name: 'msapplication-config',
				content: '/media/favicons/browserconfig.xml',
			},
			{ name: 'theme-color', content: '#ffffff' },
		],
		link: [
			{
				rel: 'apple-touch-icon',
				sizes: '180x180',
				href: '/media/favicons/apple-touch-icon.png',
			},
			{
				rel: 'icon',
				type: 'image/png',
				sizes: '32x32',
				href: '/media/favicons/favicon-32x32.png',
			},
			{
				rel: 'icon',
				type: 'image/png',
				sizes: '16x16',
				href: '/media/favicons/favicon-16x16.png',
			},
			{ rel: 'manifest', href: '/media/favicons/site.webmanifest' },
			{
				rel: 'mask-icon',
				href: '/media/favicons/safari-pinned-tab.svg',
				color: '#111111',
			},
			{ rel: 'shortcut icon', href: '/media/favicons/favicon.ico' },
		],
	},

	// Global CSS (https://go.nuxtjs.dev/config-css)
	css: ['~/assets/style.scss'],

	// Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
	plugins: ['~/plugins/formatDate.js'],

	// Auto import components (https://go.nuxtjs.dev/config-components)
	components: true,

	// Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
	buildModules: [
		// https://go.nuxtjs.dev/eslint
		process.env.NODE_ENV === 'production' ? '@nuxtjs/eslint-module' : null,
		// https://go.nuxtjs.dev/stylelint
		process.env.NODE_ENV === 'production' ? '@nuxtjs/stylelint-module' : null,
	].filter(Boolean),

	// Modules (https://go.nuxtjs.dev/config-modules)
	modules: [
		// https://go.nuxtjs.dev/content
		'@nuxt/content',
		// Paginate before i18n so routes get internationalised as necessary
		[
			'~/modules/paginate-routes',
			{ paths: ['/posts/', '/creations/', '/creations/medium/:medium?/'] },
		],
		'nuxt-i18n',
		'@nuxtjs/feed',
		'~/modules/no-nuxt-client',
	],

	// Content module configuration (https://go.nuxtjs.dev/config-content)
	content: {
		liveEdit: false,
		markdown: {
			remarkPlugins(plugins) {
				return ['remark-hreflang', ...plugins]
			},
			rehypePlugins(plugins) {
				return [
					...plugins,
					'./rehype/well-known-urls',
					'./rehype/code-blocks',
					'./rehype/format-code',
					'rehype-hreflang',
					['rehype-highlight', { ignoreMissing: true }],
				]
			},
			highlighter(rawCode, lang, _, { h, node, u }) {
				// Simply returning `rawCode` would get any HTML in there parsed
				// Instead, we need to create the HAST structure
				// and pass the code as a text node
				return h(node, 'pre', [
					h(node, 'code', { className: [`language-${lang}`] }, [
						u('text', rawCode),
					]),
				])
			},
		},
	},

	// Remove nuxt client
	noNuxtClient: {
		removeNuxt: {
			ignore: (node) =>
				EXTRA_JS && /(runtime|main).js/.test(node.properties.src || ''),
		},
	},

	// Build Configuration (https://go.nuxtjs.dev/config-build)
	build: {
		extractCSS: true,
		extend(config, { isClient, isDev }) {
			if (isClient && EXTRA_JS) {
				config.entry = Object.assign({}, config.entry || {}, EXTRA_JS)

				if (isDev) {
					// Thanks Nuxt default config
					// https://github.com/nuxt/nuxt.js/blob/dev/packages/webpack/src/config/client.js
					const {
						options: { router },
					} = this.buildContext

					const hotMiddlewareClientOptions = {
						reload: true,
						timeout: 30000,
						path: `${router.base}/__webpack_hmr/${config.name}`.replace(
							/\/\//g,
							'/'
						),
						name: this.name,
					}

					const hotMiddlewareClientOptionsStr = querystring.stringify(
						hotMiddlewareClientOptions
					)

					config.entry.main.unshift(
						'eventsource-polyfill',
						`webpack-hot-middleware/client?${hotMiddlewareClientOptionsStr}`
					)
				}
			}
		},
	},

	hooks: {
		async 'content:file:beforeInsert'(document) {
			const languageCodes = LOCALES.locales.map((l) => l.code)
			const regexp = new RegExp(`--(${languageCodes.join('|')})$`)

			const languageInfo = detectLanguage(document.path, {
				languages: languageCodes,
			})
			Object.assign(document, languageInfo)
			document.slug = document.slug.replace(regexp, '')

			document.route = join(dirname(document.path), document.slug)
				.replace(/^\//, '')
				.replace(/index$/, '__INDEX__')

			// Set default date if not date is set
			if (!document.date) {
				try {
					document.date = new Date(document.slug).toISOString()
				} catch {
					document.date = document.createdAt || new Date().toISOString()
				}
			}

			if (document.dir === '/creations' && document.extension === '.yml') {
				document.metadata = await Image(`./content${document.path}.jpg`, {
					outputDir: 'assets/images',
					widths: [960],
					formats: ['webp', 'auto'],
				})
			}
		},
	},

	i18n: {
		...LOCALES,
		vueI18nLoader: true,
		vueI18n: {
			fallbackLocale: 'en',
			messages: {
				en: {
					siteDescription: 'Thoughts about front-end development (mostly)',
					rssFeed: 'RSS feed',
					creationMedium: {
						any: 'Any',
						sketch: 'Sketch',
						painting: 'Painting',
						digital: 'Digital',
						pottery: 'Pottery',
					},
				},
				fr: {
					siteDescription: "Pensées sur l'intégration web (en gros)",
					rssFeed: 'Flux RSS',
					creationMedium: {
						any: 'Tous',
						sketch: 'Croquis',
						painting: 'Peinture',
						digital: 'Numérique',
						pottery: 'Poterie',
					},
				},
			},
			dateTimeFormats: DATE_TIME_FORMATS,
		},
		seo: true,
		vuex: {
			syncLocale: true,
			setRouteParams: true,
		},
	},

	feed() {
		const baseUrl = SITE_URL
		const feedFormats = {
			rss: { type: 'rss2', extension: '.rss' },
			json: { type: 'json1', extension: '.json' },
		}
		const { $content } = require('@nuxt/content')

		const feeds = LOCALES.locales.map(createFeeds).flat(3)
		return feeds

		function createFeeds(locale) {
			return Object.values(feedFormats).map(({ extension, type }) => [
				{
					path: i18nRoute('posts' + extension, { locale: locale.code }),
					type,
					create: createFeedArticles(locale.code),
				},
				{
					path: i18nRoute('creations' + extension, { locale: locale.code }),
					type,
					create: createFeedForCreations(locale.code),
				},
			])
		}

		function createFeedForCreations(localeCode) {
			return async function create(feed) {
				feed.options = {
					title: 'Romaric Pascal',
					author: 'Romaric Pascal <hello@romaricpascal.is>',
					description: CREATIVE_FEED_DESCRIPTIONS[localeCode],
					link: makeUrlAbsolute(
						`/${i18nRoute('creations', { locale: localeCode })}/`,
						baseUrl
					),
					language: localeCode,
				}

				const creations = await $content('creations')
					.where({
						extension: '.yml',
					})
					.sortBy('date', 'desc')
					.limit(20)
					.fetch()

				const feedItems = await Promise.all(
					creations.map(async (article) => {
						const path = i18nRoute(article.route, {
							locale: localeCode,
						})
						const url = makeUrlAbsolute(`/${path}`, baseUrl)

						const page = await readFile(
							join('dist', path, `index.html`),
							'utf-8'
						)

						// Update
						const $ = Cheerio.load(page)
						makeUrlsAbsolute($, baseUrl)
						const content = $('main').prop('innerHTML')

						// Format the title of the article
						const dateLocale = localeCode === 'en' ? 'en-gb' : localeCode
						const format = new Intl.DateTimeFormat(
							dateLocale,
							DATE_TIME_FORMATS[dateLocale].short
						)
						const date = new Date(article.date)

						return {
							title: format.format(date),
							id: url,
							link: url,
							date,
							content,
						}
					})
				)

				// Add only after each item is processed so the order is preserved
				feedItems.forEach((feedItem) => feed.addItem(feedItem))
			}
		}

		function createFeedArticles(localeCode) {
			return async function create(feed) {
				feed.options = {
					title: 'Romaric Pascal',
					author: 'Romaric Pascal <hello@romaricpascal.is>',
					description: FEED_DESCRIPTIONS[localeCode],
					link: makeUrlAbsolute(
						`/${i18nRoute('posts', { locale: localeCode })}/`,
						baseUrl
					),
					language: localeCode,
				}
				const articles = await $content('posts')
					.where({
						language: localeCode,
					})
					.sortBy('date', 'desc')
					.limit(20)
					.fetch()

				const feedItems = await Promise.all(
					articles
						.filter((post) => !post.draft)
						.map(async (article) => {
							const path = i18nRoute(article.route, {
								locale: localeCode,
							})
							const url = makeUrlAbsolute(`/${path}`, baseUrl)

							const page = await readFile(
								join('dist', path, `index.html`),
								'utf-8'
							)

							// Update
							const $ = Cheerio.load(page)
							makeUrlsAbsolute($, baseUrl)
							const content = $('main').prop('innerHTML')

							return {
								title: article.title,
								id: url,
								link: url,
								date: new Date(article.date),
								content,
							}
						})
				)

				feedItems.forEach((feedItem) => feed.addItem(feedItem))
			}
		}
	},

	router: {
		trailingSlash: true,
		extendRoutes(routes) {
			// Duplicate the catch all route to create a root route
			// This will allow the i18n plugin to correctly
			// create the internationalised route paths
			const root = routes.find((r) => r.name === '*')
			routes.splice(routes.length - 1, 0, {
				...root,
				name: '/',
				path: '/',
				chunkName: 'pages/index',
			})
		},
	},

	generate: {
		routes: ['/playing/board-games/', '/fr/playing/board-games/'],
	},
}

// https://html.spec.whatwg.org/multipage/images.html#image-candidate-string
const IMAGE_CANDIDATE_STRING_REGEX = /\s*(?<url>\S+)\s*(?<descriptor>\S*)\s*/

const ATTRIBUTES_WITH_URLS = ['src', 'href']

function makeUrlsAbsolute($, baseUrl) {
	ATTRIBUTES_WITH_URLS.forEach((attributeName) => {
		$(`main [${attributeName}]`).attr(attributeName, function () {
			const $element = $(this)
			const url = $element.attr(attributeName)
			return makeUrlAbsolute(url, baseUrl)
		})
	})

	$(`main [srcset]`).attr('srcset', function () {
		const $element = $(this)
		const srcset = $element.attr('srcset')
		return updateSrcSet(srcset, ({ url, descriptor }) => {
			return {
				url: makeUrlAbsolute(url, baseUrl),
				descriptor,
			}
		})
	})
}

function updateSrcSet(srcset, iterator) {
	return srcset
		.split(',')
		.map((imageCandidateString) => {
			const parts = IMAGE_CANDIDATE_STRING_REGEX.exec(imageCandidateString)
			const processed =
				iterator(parts.groups, imageCandidateString, srcset) || parts.groups
			if (processed.descriptor) {
				return processed.url + ' ' + processed.descriptor
			} else {
				return processed.url
			}
		})
		.join(',')
}

function makeUrlAbsolute(url, baseUrl) {
	if (url.startsWith('/')) {
		return baseUrl + url
	}
	if (url.startsWith('./')) {
		return url.replace('./', baseUrl)
	}
	return url
}

function i18nRoute(path, { locale }) {
	if (locale !== LOCALES.defaultLocale) {
		return `${locale}/${path}`
	}

	return path
}
