---
title: En details
slug: en-details
noForHireCTA: true
---

<div>

<img src="/media/me.png" alt="" class="avatar no-border" role="presentation">

**Enchanté, je suis Romaric** et je construis des sites web depuis plus de 14 ans maintenant. En cours de route, j'en suis venu à m'intéresser de près à l'accessibilité et aux performances, qui permettent d'offrir une bonne expérience au plus grand nombre.

Je me spécialise dans l'écriture de templates ou composants sémantiques, et de CSS maintenable (de zéro ou en complément de librairies telles que Bootstrap) pour supporter des backends variés: côté serveur comme Rails ou Django, ou côté client comme React ou Vue.

</div>

Je suis également très attaché à la protection des données privées, mais je crois beaucoup au partage des connaissances, qu'il s'agisse de [code de mes projets perso][github-profile] ou de [ce que j'en apprend](#articles).

Si je ne suis pas en train de m'occuper de code, vous me trouverez surement en train de dessiner ou peindre, de jouer a des jeux de société ou videos ou encore de courir (parfois après un frisbee).

En ce moment: Amba Health and Care - Front-end lead
---

Avec l'acquisition de <a href="https://cookieshq.co.uk" hreflang="en">CookiesHQ</a> par <a href="https://amba.co" hreflang="en">Amba</a>, j'ai conservé les responsabilités de mon rôle précédent, maintenant focalisé sur un seul projet pour aider aux soins des personnes agées plutôt qu'étendu à plusieurs applications:

- conception de implémentation de composants front-end, en collaboration avec les développeurs backend, et le reste de l'equipe plus généralement
- support de l'équipe sur les problèmes front-end en pair programming ou lors de revues de code
- introduction de composants pour l'organisation du code front-end, à l'aide de <a href="https://viewcomponent.org" hreflang="en" lang="en">View Component</a>
- promotion de l'accessibilité lors du développement et du test des fonctionnalités
- préconisation de bonnes pratiques et patterns front-end intéressants lors des revues de codes et du partage d'articles techniques

<article class="project">

### Technologies

Rails, View Component, HTML (HAML), CSS (Bootstrap, SCSS), JavaScript (Turbo, Stimulus, Lit), tooling (Webpack)

</article>

J'essaie également d'ouvrir les source de parties non-sensibles du code, plus spécialement autour de l'utilisation de composants. J'espère publier les Gem Ruby aussi tôt que possible, en attendant, vous pouvez consulter leur depôts de code:

<article class="project project--side">

### [merge_attributes]

Un helper Rails pour combiner plusieurs `Hash` a utiliser comme attributs HTML, avec un traitement spécial pour `class` ou d'autre attributs, suivant configuration (comme `data-controller` ou `data-action` de Stimulus ou `aria-labelledby`).


</article>

<article class="project project--side">

### [View Component concerns][view-component_concerns]

Reflexion et modules Ruby autour de pattern récurrents lors de l'utilisation de <a href="https://viewcomponent.org" hreflang="en" lang="en">View Component</a>: présence d'un élément racine, calcul de ses attributs, calcul d'ID ou de noms de classes...

</article>

Par le passé
---

### Nov. 2017 - Jan. 2022: Cookies HQ - Développeur front-end

Conception et développement de composants front-end pour diverses applications, en collaboration avec les développeurs backend et designers. Support à l'équipe sur les problèmes front-end.

J'ai également sensibilisé à l'accessibilité, au travers de:

- <a href="https://www.youtube.com/watch?v=cv0o-2EH28Q" hreflang="en" lang="en">une presentation en ligne (video avec sous-titres, automatiques malheureusement)</a>
- l'écriture de guides internes décryptant les criteres WCAG pour les different métiers (designers, developpeurs, testeurs)
- <a href="https://cookieshq.github.io/accessibility-smart-cookies" hreflang="en">une présentation publique</a> au  <a href="https://www.meetup.com/Smart-Cookies-Bristol/events/264888812/" hreflang="en">meetup Smart Cookies</a>
- <a href="https://www.cookieshq.co.uk/posts/author/romaric" hreflang="en">divers articles</a>
- <a href="https://www.cookieshq.co.uk/posts/building-accessible-websites-is-a-job-for-the-whole-team" hreflang="en">une présentation interne</a>

<blockquote lang="en">

He was a key member of our team, leading the front-end development on many projects. He was our accessibility and ethics champion. He has challenged us more than anyone else, pushing us to constantly improve our processes and the way we approach projects.

We have learnt a lot from him. I think that, going forward, Nic and I will always have a little Romaric's voice in our heads but above all, we're very grateful for the 2 years that he has spent with us.

<cite><a href="https://www.linkedin.com/feed/update/urn%3Ali%3Aactivity%3A6638408058257055744/">Nathalie Alpi - Co Founder and Managing Director at CookiesHQ</a></cite>

</blockquote>

<article class="project">

#### <a href="https://captioned.talk-type.com" hreflang="en">Caption.Ed</a>

<p class="lead">
Système de composants pour structurer l'interface utilisateur et ses futurs développements
</p>

Pour structurer le code frontend et aider à en abstraire la complexité de certains patterns, j'ai créé un ensemble de composants utilisant <a href="https://viewcomponent.org" hreflang="en" lang="en">View Component</a> et Tailwind. De fonctionalités génériques (liens/boutons, formulaires, tables de données) jusqu'à des composant metiers specifique à l'application, j'ai pu travailler du differents niveaux d'abstractions:
- du balisage: HTML ad-hoc et/ou composition avec d'autres composants
- CSS: classes génériques pour toute l'application ou spécifiques au composant
- JavaScript: améliorations légères avec <a href="https://stimulus.hotwired.dev" hreflang="en" lang="en">Stimulus</a> ou composant "full-JS" avec Preact

<p class="tech-list">Rails, View Component, HTML (ERB), CSS (Tailwind, BEM), JavaScript (Turbo, Stimulus, Preact), tooling (Webpack)</p>


</article>

<article class="project">

#### <a href="https://stornaway.io/#homevideoplayer" hreflang="en" lang="en">Stornaway</a>

<p class="lead">
Développement d'un éditeur de graphes pour faciliter la création de vidéos interactives.
</p>

En dehors de l'implémentation générale des styles, j'ai plus particulièrement travaillé sur l'éditeur de graphe lui-même. Il permet aux créateurs/trices de cartographier leurs histoires: quelles scènes les composent, quels choix sont offerts à la fin de chacune et comment ceux-ci affectent la scène choisie. Iels peuvent ensuite uploader une vidéo pour chaque scène et exporter leur projet pour le diffuser grace au lecteur interactif de l'application.

Plus particulièrement:

- Prototypage de la faisabilité des fonctionnalités à ajouter à la bibliothèque de graphes (JointJS/RappidJS)
- Collaboration avec les développeurs back-end sur l'architecture pour sauver et charger les graphes
- Intégration de l'édition de graphe avec des formulaires Rails pour éditer les données des nœuds
- Implémentation de contrôles de forulaires spécifiques (onglets, upload, color-picker)
- Elargissement des fonctionnalités existantes du lecteur interactif
- Développement d'une extension navigateur pour automatiser l'intégration avec YouTube Studio

<p class="tech-list">Rails, HTML (HAML), CSS (Bootstrap, SASS, SCSS), JavaScript (RappidJS), tooling (Webpack)</p>

</article>

<article class="project">

#### <a href="https://bigcleanswitch.org" hreflang="en" lang="en">Big Clean Switch</a>

<p class="lead">
Développement d'un "éditeur de blocs" pour aider la création de variantes d'un comparateur de fournisseurs d'énergie renouvelables.
</p>

Pour aider Big Clean Switch à adapter leur formulaire aux utilisateurs/trices ainsi qu'à leurs partenaires, j'ai participé à:

- l'implémentation d'un "éditeur de blocs" qui leur permet d'organiser des champs prédéfinis en différentes étapes et de choisir un thème de couleurs adapté à leurs partenaires commerciaux,
- l'export du formulaire resultant pour l'intégrer à leur site Wordpress ou un site externe, en minimisant l'impact au performance (au travers de JS "vanilla" et de CSS spécifiques) et en évitant aux styles extérieurs d'affecter le formulaire.

Notamment:

- Implémentation sémantique de divers champs de formulaire
- Écriture de styles CSS responsive et thémable pour leur mise en page
- Réalisation de la navigation entre étapes par JavaScript ainsi que de comportements spécifiques aux champs (gestion d'erreurs, visibilité, adaptation des valeurs aux choix...)
- Sélection de styles (choix de thémes)
- Collaboration avec les développeurs backend, à la fois sur le design et l'implémentation, à la sauvegarde, au rendu et la soumission du formulaire
- Intégration du formulaire sur le site marketting, ainsi que sites tiers
- Intégration d'un éditeur d'options réalisé avec Vue avec des formulaires Rails dans l'administration

<p class="tech-list">Rails, HTML (HAML), CSS (SASS, PostCSS), JavaScript (Vanilla), tooling (Webpack)</p>

</article>

<article class="project">

#### <a href="http://patternbank.com" hreflang="en">Patternbank</a>

<p class="lead">
Développement d'un "éditeur de blocs" pour faciliter la création de pages de contenu en support d'un site ecommerce.
</p>

Pour aider Patternbank à ajouter des pages de contenu à leur site ecommerce, j'ai participé à la construction d'un "éditeur de bloc". Il permet de construire les pages en combinant des sections configurables. L'aspect visuel fort de ce site a demandé de se pencher particulièrement sur:

- des images responsive et performantes (lazy loading, srcset, arrière plans responsive)
- proposer des mises en pages responsives variées
- offrir des choix de styles (typographie, couleurs, variantes de composants)
- permettre une typographie fluide, mais accessible
- donner le contrôle sur les niveaux de titre.

D'un point de vue plus technique:

- Réalisation d'un prototype côté client pour explorer efficacement le concept avec Vue
- Collaboration, à la fois sur le design et l'implémentation, avec les développeurs backend pour sauver et charger le contenu
- Implémentation des composants et du rendu des page
- Écriture de styles
- Intégration d'un éditeur d'options réalisé avec Vue avec des formulaires Rails dans l'administration

<p class="tech-list">Rails, HTML (HAML), CSS (SASS, PostCSS), JavaScript (Vue, Vanilla), tooling (Webpack)</p>

</article>

<article class="project">

#### Mise à jour de sites ecommerce existant

<p class="lead">
Implémentation de changements de design et nouvelles fonctionnalités à des sites ecommerce en production
</p>

Pour des sites comme <a href="https://www.goodsixty.co.uk/retailers/bristol/115-earthcake" hreflang="en" lang="en">Good Sixty</a>, <a href="https://patternbank.com/studio" hreflang="en" lang="en">Patternbank</a> ou <a href="https://www.roughtrade.com/gb/s" hreflang="en" lang="en">Roughtrade</a>, j'ai implémenté les changements d'interface utilisateur/trice, notamment:

- liste de produits
- formulaire de filtrage
- pages produits
- checkout

<p class="tech-list">Rails, HTML, CSS (SASS), JavaScript (jQuery, Turbolinks)</p>

</article>

### Mar. 2020 - Sep 2020: Explorations personelles

Je suis parti de CookiesHQ de côté quelques temps pour des expérimentations personelles. J'ai développé [quelques expériences](#autres-projets) mais aussi des projets plus aboutis:

<article class="project">

### [Site personel][romaricpascal.is]

<p class="lead">Explorer la génération de site statiques, l'internationalisation et la production d'articles</p>

Repartant de zéro, j'ai reconstruit ce site en utilisant un générateur de site statique (Metalsmith), implémentant des fonctionnalités pour publier à la fois en français et en anglais. Des [articles sur le blog][/fr/posts/] documentent en détail le processur, du JavaScript pour implémenter les fonctionnalités aux CSS mis en place pour les styles, en passant par des bases d'accessibilité.

<p class="tech-list">JavaScript (NodeJS), Metalsmith, HTML, CSS, Markdown</p>

</article>

<article class="project">

### [To/Fro][to-fro]

<p class="lead">Collaboration bénévole à une application pour coordonner les volontaires de <a href="https://knowlewestalliance.co.uk/" hreflang="en" lang="en">Knowle West Alliance</a> qui aident les personnes isolées par la crise du Covid-19</p>

J'ai participé au hackathon <a href="https://www.euvsvirus.org/" hreflang="en">EUvsVirus</a> qui a vu le démarrage du projet et ai continué sa construction après. L'équipe étant très réduite, j'ai pu participer sur des aspects variés: mise en place d'outils pour le front-end, implémentation de templates Django et leur styles avec Bootstrap, mais aussi utilisation de l'ORM de Django pour requêter les données à afficher, personnalisation de l'admin Django, de l'authentification et des e-mails.

<p class="tech-list">Django, HTML, CSS (Bootstrap, SASS), JavaScript (jQuery), tooling (Parcel)</p>

</article>

### Avant Nov. 2017

- Jan 2016 - Nov 2017: Artiste lettreur indépendant
- Jan 2012 - Dec 2016: Développeur front-end et web designer indépendant - Travaux incluant la réalisation d'un interface utilisateur/trice pour le <a href="https://community.hpe.com/t5/behind-the-scenes-at-labs/introducing-loom-a-new-tool-for-managing-complex-systems/ba-p/6793962" hreflang="en">projet Loom de HP Labs</a>
- Avr. 2008 - Aout 2011: Atos Worldine - De stagiaire à lead-developer sur l'API de cartes du Géoportail de l'IGN

Autres projets
---

En dehors de travaux pour des clients, et de [ce site][romaricpascal.is], je bricole aussi sur différents projets: petites applications, outils ou juste expérimentations avec des concepts ou technologies qui me semblent intéressantes. En voici quelque uns:

<article class="project project--side">

### <a href="https://harvrest.com" hreflang="en">Harvrest</a>

Une <a href="https://gitlab.com/romaricpascal/harvrest" hreflang="en">application pomodorro réalisée avecSvelte (sources sur Gitlab)</a> ou le temps de repos augmente si l'on dépasse les 25 minutes de travail.

</article>

<article class="project project--side">

### <a href="https://taskfight.romaricpascal.com" hreflang="en">Taskfight</a>

Une <a href="https://gitlab.com/romaricpascal/taskfight" hreflang="en">application (plus toute jeune) React (sources sur Gitlab)</a> où les tâches se confrontent les une aux autres pour décider de leur priorité.

</article>

<article class="project project--side">

### <a href="https://gitlab.com/romaricpascal/express-mount-files" hreflang="en">express-mount-files</a>

Un middleware ExpressJS pour déclarer les routes avec des fichiers et dossiers, nostalgie des jeunes jours de PHP.

</article>

<article class="project project--side">

### <a href="https://gitlab.com/romaricpascal/implicitjs" hreflang="en">implicitjs</a>

Détourner la syntaxe de JavaScript avec Babel pour essayer d'en faire un langage de template. Extrêmement expérimental.

</article>

<article class="project project--side">

### <a href="https://gitlab.com/romaricpascal/bezier-spline-experiment" hreflang="en"> Une expérience sur les courbes de Bézier</a>

Une application Svelte qui illustre une idée pour dessiner une courbe de Bézier passant par une liste de points.

</article>

Publications
---

J'ai beaucoup appris de ce que les autres ont partagé. Il me semble donc normal d'essayer de rendre la pareil (avec un succès mitigé sur la régularité):

- [sur ce site](/fr/posts/): Pour le moment, un pas à pas de la reconstruction de ce site
- <a href="https://www.cookieshq.co.uk/posts/author/romaric" hreflang="en">sur le blog de CookiesHQ</a>: Conseils sur l'accessibilité et les performance lorsque j'y étais employé
- <a href="https://old.romaricpascal.is/writing-about/">sur mon site précédent</a>: Mélange d'articles sur le lettrage et le développement web.

[github-profile]: https://gitlab.com/romaricpascal/
[to-fro]: https://github.com/cgillions/to-fro/
[romaricpascal.is]: https://gitlab.com/romaricpascal/romaricpascal.is
[caption.ed]: https://captioned.talk-type.com/
[View Component]: https://viewcomponent.org
[Tailwind]: https://tailwindcss.com/
[Stimulus]: https://stimulus.hotwired.dev/
[CookiesHQ]: https://cookieshq.co.uk
[amba]: https://amba.co
[merge_attributes]: https://github.com/Amba-Health/merge_attributes
[view-component_concerns]: https://github.com/Amba-Health/view_component-concerns
