---
draft: true
title: Enhancing a page with JavaScript
---
Enhancing a page with JavaScript
===

Not talking about pages where a framework heavily re-hydrates its pre-rendered content. But more of a traditional approach of SSR + "sprinkling some JS on top".

There are a couple of ways you can run some code to enhance your page, each providing a different lifecycle.

Targetting elements
---

tl;dr: Use `data-attributes`, or `js-` classes. Though the former have some advantages.

Before diving into the ways code gets executed, I want to make a quick not on how to pick elements on the page.

You'll want some kind of convention for that, usually relying on either:

- classes, in which case I'd encourage to have clear classes dedicated to attaching JavaScript (a common convention is to prefix them with `js-`). This makes things easier when comes the time to remove a class from the element. If it doesn't start with `js-` no worrying that it'd also remove some behaviour brought by JavaScript.

A disclosure widget could be hooked on the following, for example:

```html
<!-- 
  Note the lack of aria attributes necessary for the disclosure
  widget on the button.
  This is not for simplification, but because those should be
  added by the JavaScript as they'll only be relevant if the
  JavaScript is actually running
-->
<button class="js-disclosure">More details</button>
<div>
The content that'll be hidden or revealed.
</div>
```

- data attributes actually provide another good way to iden

Running some code once
---

Pick element, do stuff with it, for example add a `.js` (or remove a `.no-js` class).

React to events
---

Using MutationObservers
---

Web Components
---
