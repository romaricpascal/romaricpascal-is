---
title: Before implementing front-end features…
date: 2021-03-17
draft: true
type: post
layout: post.pug
---
… I try to ask a few questions (to myself and the team I work with, if they hold the answer or things need further discussion). Most of the answers will already have been uncovered through the design stages, so it's not about doing extra thinking. Of course, there will be missed bits here and there, both in answers not thought about yet or questions forgotten to be asked. We're only humans building software, after all. But asking some questions ahead of starting help plan the implementation and limit surprises in the middle of coding.

The big picture - which are the states and interactions?
---

Most features will involve users going through different states of the site/application. Some full screens, other smaller components, for example:

- maybe adding an item to a cart will have you go through a product page showing an "add to cart" button to one where it has "plus" and "minus" buttons to adjust the quantities and an updated count of items over the cart link
- or perhaps an upload field will have you go from picking a file, to watching the upload progress and finish showing the file has been successfuly uploaded

In between each state is an interaction, maybe from the user (they activated the "add to cart" button), but also from the machine (the upload finished).

That overview highlights the big steps for the implementation. They can be further divided, the states in smaller sections/components or the interactions in smaller steps, if relevant.

### Is it a well identified pattern?

The W3C documents how to implement a number of design patterns and widgets in the WAI-ARIA authoring practice. If the interactions seems close to one of those, it's worth identifying how the feature is deviating and what would need adressing before starting to code.

From there, time to look more closely into the details of both states and interactions (or their smaller division).

States - What's on there?
---

### What content is in each state?

Content is the foundation of any web page. Just like we needed an inventory of those states, we need one for their content if we are to make sure nothing is missing: a bit of text here, over there a heading, there goes an image, here is a form with fields and button(s)…

These can start very low level and precise, but as the project grows, there'll likely be some abstractions emerging that will speed up identifying some of the content. Instead of listing a date, title and excerpt, you might just list a "blog entry summary".

### How does the content vary?

There will always be variation, if only in the length (small words, big words, sentences, paragraphs), size of images. But those might be larger like:

- parts are not necessarily present: this blog entry does not have any tags, that form fields show a hint
- parts shown differently: the price of that service is discounted and needs to show both previous and current price, that card highlights that the product is featured, that form field has an error

### Which semantics to give the content?

Semantic markup makes sure everybody can understand what's on the page, regardless of their way of accessing it. So once the content is listed, variations included, we need to decide which semantics to give to each piece of content. Their role, but also their name and states, depending on the situation.

- this bit of text, is it a heading, the label of a field, the heading of a table?
- is that section a landmark? of which type? with which label?
- this button only shows an icon, what's its accessible name? (oh, a new peice of content!)
- do these images need content for their `alt` tag? which? (oh, a new piece of content too!)
- that state shows a loading spinner, how will convey that? (that could be some more content too if we want some accessibly hidden text).

All these question will inform the markup involved, and we can give a think about their styling.

States - How are they rendered?
---

### What's the content layout and how does it reflow?

Going from a broad view to details, the first thing is the layout of the content. Where is each piece of content, how big is it and what controls its size, how is it spaced from the other pieces of content around it?

A key thing to think about here is how does the layout responds to the different constraints:

- viewport size (responsiveness) for starters, thinking about the width (especially those in between narrow/mobile and wide/desktop, often left out and in need of answers), but also height or aspect-ratio if necessary
- content variations will also affect layout, what if that text is longer, that image not the right aspect ratio, that bit missing…

Although this should have been taking into account during design, giving a thought at the accessibility of the layout is good to do before starting: is the content laid out in an order that makes sense, are the interactive elements big enough/spaced enough to interact with,… Best to discuss adjustments before being knee deep in the code.

### How does it look?

Rendering is not just a matter of putting things in the right place. Each part of the content needs to look the right way: right font, right colors, right background, shadows... We need to take stock of those things that don't move things around (too much, fonts do affect layout after all).

Responsiveness is again to take into account here. It'll have less effect than on the layout, but may still affect font-sizes, background-images (we don't want to load an HD background on mobile, do we?)…

This part is not just about identifying the styles themselves (this text is set in this font at that size, this element in this color on that background...), but what they represent (this means the product is premium, this means there is an error). This will inform how to name and structure classes.

Similarly to the layout, a final check around accessibility is useful there: are the colors contrasted enough (for text, graphic elements & controls, focus styles), is there other elements than colors to convey meaning…

### Are there patterns ready to be reused or abstracted?

Some parts are bound to repeat across the site, others might follow well known patterns that have already been built (and named) again and again. Identifying them and building them for reuse will keep things tidy and speed things in the long run.This can range from design tokens (colors, fonts...), small bits of UI (buttons, pills, form field), all the way to larger components.

Two things to keep in mind:

- when picking an abstraction to reuse, you need to make sure it matches what you're actually trying to do. If you have to bend it or override most of it for the feature you're building, maybe it's not the right abstraction to pick and having a separate bit of code will be better
- when deciding to abstract a pattern into something more generic, you need to make sure it'll bring value in the long run. How many `.card` classes tie different parts of sites to the same base styles unnecessarily, making any update to it a minefield because of the amount of places that would be impacted.

Interactions - how do users move from state to state?
---

### How does it start, how does it end?

Before thinking of what actually happens during the interaction,
it's worth looking at where it starts and where it ends. Not the overal states, but the specific parts that are impacted by the interaction.

At the starting point:

- the element/component the interaction starts from, to make sure it has the right semantics and styles (hover & focus styles, for ex.)
- the action that'll trigger the interaction, and if it's something that requires gesture/hover which alternatives are planned for users to achieve the same outcome with a series of "one touch"
- the immediate feedback users will get from triggering the action (active state, animation…)

At the end point:

- the elements/components that were affected by the interaction: what was added, removed or update from the start screen. If the end state is a variation of the start state, it can be important to identify what happened
- what provides feedback to users about the outcome of their action (flash message, animation), especially how to make sure it's conveyed properly both visually and to assistive technology

### Without JavaScript, what happens?

Before relying on JavaScript to enhance the user experience, planning the interaction to run through native browser features allows to provide a robust experience.

It also helps inform the semantics of the elements involved in the interaction. Maybe the section wrapping that button actually needs to be a form that'll get natively submitted unless JavaScript takes over.

This step usually also highlights the core of data that needs to be exchanged with the server for the interaction to happen.

### With JavaScript, what happens?

Send/receive data, but not just.
Loading state
Error handling
Concurrency

Source
---

Where does all that's on the screen comes from?
