---
title: "From mockup to code: planning the work"
date: 2022-02-20
draft: true
---
Let's say you're building an ecommerce site, and it's time to build its front-end. The listing page, more precisely, with all the products, each with a nice "Add to basket" button. Something that looks somewhat like this:

IMAGE: initial state

Once you click the button, the button will turn into "+" an "-" buttons to adjust the quantities and the cart will update accordingly.

IMAGE: final state.

Before jumping in the code, it pays to do a thorough review to not be caught with nasty surprise while coding. We'll leave aside the cart bit and focus just on the product card for this article. There'll already be plenty to see.

Content
---

First step is taking a solid inventory of the content that's displayed on the screen. For our product cart, we can identify 4 piece of content:

1. the image
2. the name
3. the price
4. the "Add to basket" button
5. the quantity in basket

### Variations & Constraints

Each card is not displaying the same product, so there'll be variations in each of the piece of content identified before. Looking for how each piece of content can vary will make the implementation as robust as it can be.

1. the image

- may be not filled in the admin
- may not have been uploaded with the right aspect ratio

2. the name

- may be a short word, a long word, a couple of short words, a longer combination of words

3. the price

- may be full, or discounted

4. the "Add to basket" button

- is shown only when the product is not yet in the basket

5. the quantity stepper

- is shown only when the product is already in the basket

### Source

The content is not pulled out of thin air and we need to know where it comes from. This can be:

- a database or external API, like the majority of the card (name, image, price, quantity in cart), often worth a check that the piece of information can indeed be provided/computed by those sources
- a configuration or language file, which could be the case for the "Add to basket" message, helping consistency across the site or future internationalisation
- hardcoded in the template, another option for the "Add to basket" message. Plain and simple.

### Semantics

Before moving to how the content is presented, there's one last stop regarding the content itself: looking into its semantics. This'll inform which HTML elements to use for the implementation.

1. the image: does it need alternative content? It depends... Given we're on a listing, that the image is before the heading and if the people inputing the content are only likely to provide "Picture of PRODUCT NAME" as alternative text, it's probably best to leave it as a decorative image with no alternative content (ie. an empty alt attribute).
2. the name: it's definitely a heading, that'll help people navigate quickly through the page
3. the price: strikethrough text won't be presented any differently to assistive technology, so it'll need a little extra labelling to ensure everyone understand what was the previous price and what's the new one
4. the buttons: because of there'll be many "Add to basket", "+" or "-" buttons on the page, they'll need disambiguation from one another so everyone knows which product they're adding or removing from their basket.

Now we have a thorough set of requirements for the content, we can look into the styling.

Styles
---

### The concerns of CSS

Just like for the content, it pays to have a clear picture of the styles to implement. These can be split into 4 categories:

1. Layout: How each element is positioned and spaced relative to the others/their parent
2. Structure: Padding, borders and possible width and height that "belong to" the element
3. Typography: Font family, styles and sizes
4. Appearance: Anything that doesn't alter the 3 previous categories. That'll be things like colors, opacity, shadows...

### Responsiveness

For each of those categories, we need to make sure the component responds appropriately:

- to inside constraints: the image not being the right ratio, the name being too long
- to outside constraints: how does the font size adapt to screen size, does the content get re-ordered (pref. not), what happens in the often-forgotten "in between" states (esp. between mobile width and table, as well as landscape mobile). But it's not just screen sizes. Because of the layout, what happens when the cards is taller than its content warrant, for example we'll want to push the button down to keep everything aligned.

### Systemize

Using a system to mutualise spacing, colors, font-sizes, shadows and other design tokens help ensure consistency throughout the product. So does factorising CSS into repeatable patterns (grid system, "media" layout, vertical rythm classes).

Before injecting new values in the codebase, it's worth checking what's already present. This doesn't mean loading the HTML with utility classes, there's also ways to reuse those values in SASS.

Interaction
---

Now for the more active part: the interaction.

### Where does it start?

To begin at the beginning we need to answer: where does it start from? This brings further constraints both on the content and styles side for that element:

- having the right semantics
- neet to have appropriate states styles (`:hover`, `:focus`, `:active`)

###  Accessibility at the start

Besides the semantics, there's two things to check regarding the interaction

- is the starting element reachable? especially on touch device, by keyboard and through assistive tech shortcuts.
- is the interaction workable through actionning single elements? Dragging or gestures are all handy, but will prove difficult to some users.

Answering "no" to either question doesn't mean the interaction need to be scrapped completely. Just to make sure that unless absolutely necessary for things to be done that way, there's another route for people to accomplish the same action, with the same options being provided. This might call for extra design work and/or extra planning for a future iteration (though best if sorted in that one).

###  How does it succeed?

That part is usually well covered. Here, after adding to the basket succeeds, the page updates the following way:

- the cart is updated with the new product (what this means exactly will likely be spec'ed on the cart icon),
- a notification is shown,
- the product now shows the quantity and "+"/"-" buttons.

### How can it error?

That part is often forgotten. As soon as you send a request to the server, it can error:

1. because of a network failure
2. because of a server failure

On top of that, there's all the business rules that can make things error. For example here, maybe the product is no longer available. How should that be presented to the user?

### How should it work without JavaScript?

For the "Add to basket", the path is pretty clear: post a form to the server and tada! That'll actually leave the responsibility of showing the errors to the browser for #1 and our generic 500 page for #2.

### How should it work through JavaScript?

Now that's usually the part full of surprises. Already the part about the business error might have been missing from the design, but looking deeper at how things work with JavaScript usually highlights a couple extra things to take care of.

It all sparks from this single question: is the action instant or long? Most often it'll be long (like sending a request to the server). And this leads to
two things to consider:

1. how do we show it to users?
2. how should multiple interactions be handled?

Both are handled by the browser when posting a native form. Because we take over we now have to do it ourselves.

####  Handling multiple interactions

# 2 informing #1, let's start with this one

After clicking "Add to basket", it sounds pretty reasonable that I should be prevented from taking that action again while the request is underway.

Now let's zoom out. There's multiple products on the page, should I be able to click "Add to basket" for one of the other products? "No" is the easy to implement answer: prevent all buttons to be actioned. If you say "Yes", you now need to consider that you have two requests, flying to the server, coming back updating the page... potentially not in the order they were made. And that needs adressing, either by accepting the odds of showing an inconsistent UI to the users, or finding a way to guarantee the order of responses, as well as handling when only one of the two errors.

Zooming out a bit more, there's the filtering box, which may also send requests in the background updating the list of products. And cause the same kind of crossfire in case of network latency for one of the requests. Again, a choice to make between how much to prevent and the odds of it happening.

Zooming out again, if the project has Turbolinks, same possible crossfire. Though at the level of the page, you'll likely want to abort all the pending requests.

Whichever way, there will be actions prevented on the page. This can be done multiple ways: disable one or several buttons (default), or prevent a form from submitting and showing some feedback (better).

#### Showing the loading state

Often missing from the designs (especially with the handling of multiple operations coming as a surprise), we need to know how the loading state will be shown to user. Is it just the button updating? If we prevent adding other products, an overlay over the whole list of products? Maybe the whole main area if we prevent filtering too?

Whichever way we need to make sure that:

- the loading state is announced properly by assistive technology
- if we're partially overlaying things on top of the screen (for ex. covering the list of products), those need to become inaccessible to keyboard and assistive technology just like they are for the mouse.

#### Handling the completion

Without JavaScript, the browser takes care of two other things for us:

- moving the focus (to the start of the page or whichever element has `autofocus`)
- announcing the new page

And it's now up to us to handle that. After actioning the "Add to basket" with their keyboard, where will users be on the page? How will they know that the interaction has completed (and how).
