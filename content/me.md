---
title: About me
heading: HTML/CSS Developer
noForHireCTA: true
---

<div>

<img src="/media/me.png" alt="" class="avatar no-border" role="presentation">

<span class="font-size--larger">Hello, I'm Romaric</span> and I've been building websites in **Bristol, UK** for more that **10 years now**. Along the way, I've come to care about **accessibility and performance** to offer a good experience to the widest audience.

I focus on writing **semantic templates and components**, as well as **maintainable CSS** (from scratch or on top of libraries like Bootstrap or Tailwind), in support of various backends whether server-side like **Rails or Django** (adding enhancements with **vanilla JavaScript**), or client-side like **React, Vue, Svelte or Lit**.

</div>

Professional journey
---

### Since Nov. 2017: Front-end lead at CookiesHQ, now Amba

At Bristol based agency [CookiesHQ](https://cookieshq.co.uk), now acquired by one of their client [Amba](https://amba.co), I've been responsible for:

- conceiving and implementing front-end components, in coordination with backend developers, designers and the rest of the team more generally
- structuring and documenting the company's approach to front-end with CSS architecture and guidelines (naming, organisation, working with Bootstrap) and componentisation of markup
- promoting accessibility at all stages of the projects as well as outside the company (WCAG break-down documents provided to contracted designers, [presentation at meetups](https://youtu.be/cv0o-2EH28Q))
- support the team on front-end issues through pairing, code review and sharing of technical articles

<article class="project">

#### Projects

- introduction of components (View Component) for elderly care app [Amba](https://amba.co)
- design system to structure the front-end and abstract some of its complexities for [Caption.Ed](https://captioned.talk-type.com/) (View Component + Tailwind)
- SVG based graph editor synced to HTML forms for interactive film making app [Stornaway.io](https://www.stornaway.io/#homevideoplayer) (Bootstrap + SCSS custom style, RappidJS (BackboneJS))
- build block editors to create [embeddable energy provider forms](https://bigcleanswitch.org) or content pages alongside a [pattern marketplace](https://patternbank.com) (Vue for the editor, component-like Rails templates + SCSS)

<p class="tech-list">
Rails, Bootstrap + SCSS, light JavaScript enhancements (vanilla or jQuery) but also component libraries like Vue, Preact or Lit when necessary.
</p>

</article>

<section class="no-break--inside">

As I left CookiesHQ in Mar. 2020 (before returing in Nov.), they published this kind comment on LinkedIn:

<blockquote class="margin-top--sm">

He was a key member of our team, leading the front-end development on many projects. He was our accessibility and ethics champion. He has challenged us more than anyone else, pushing us to constantly improve our processes and the way we approach projects.

<cite><a href="https://romaricpascal.is/redir/chq-linkedin-quote">Nathalie Alpi, Co Founder and Managing Director at CookiesHQ</a></cite>
</blockquote>

</section>

<section class="no-break--inside">

### Previously: 

<dl>
    <div class="no-break--inside">
        <dt>Mar. 2020 - Sep. 2020</dt>
        <dd>Personal explorations including re-implementation of this website and pro-bono collaboration of an <a href="https://github.com/bristolapproach/to-fro">app for coordinating volunteers helping isolated people during Covid-19 crisis</a></dd>
    </div>
    <div class="no-break--inside">
        <dt>Jan. 2016 - Nov. 2017</dt>
        <dd>Independent lettering artist, <a href="https://old.romaricpascal.is">designing logos, t-shirts and prints/illustrations</a></dd>
    </div>
    <div class="no-break--inside">
        <dt>Jan. 2012 - Dec. 2016</dt>
        <dd>Independent front-end developer and web designer - Works included building a JavaScript UI for HP Labs' Loom.</dd>
    </div>
    <div class="no-break--inside">
        <dt>Apr. 2008 - Aug. 2011</dt> 
        <dd>Atos Wordline - Intern to Lead developer on the Géoportail mapping API for the French Institut géographique national.</dd>
    </div>
</dl>

</section>

<section class="no-break--inside">

On the side
---

Time allowing, I explore ideas and new technologies through side projects and share about what I learn

<article class="project project--side">

### Side projects

<dl>
    <dt><a href="https://romaricpascal.is" data-no-print-url>https://romaricpascal.is</a></dt>
    <dd>Personal website for experimentation and blogging. Built on NuxtJS and custom CSS (SCSS)</dd>
    <dd>Sources: <a href="https://gitlab.com/romaricpascal/romaricpascal-is" data-no-print-url>https://gitlab.com/romaricpascal/romaricpascal-is</a></dd>
    <dt><a href="https://harvrest.com" data-no-print-url>https://harvrest.com</a></dt>
    <dd>Unfortunately named pomodoro app where rest time augments with work time. Built on Svelte + custom CSS</dd>
    <dd>Sources: <a href="https://gitlab.com/romaricpascal/harvrest" data-no-print-url>https://gitlab.com/romaricpascal/harvrest</a></dd>
    <dt>JavaStick</dt>
    <dd>Little experiment for building and thoroughly tested and documented JavaScript library. Vanilla JS tested with Cypress</dd>
    <dd>Sources: <a href="https://gitlab.com/romaricpascal/javastick" data-no-print-url>https://gitlab.com/romaricpascal/javastick</a></dd>
</dl>

<div class="tech-list prose-block">

More on <a href="https://gitlab.com/romaricpascal">Gitlab</a>

</div>

</article>

<article class="project project--side">

### Writings

<dl>
    <dt><a href="http://romaricpascal.is/posts/hover-effects-css-custom-properties/">Hover effects with CSS custom properties</a></dt>
    <dd>Exploration of CSS custom properties through the styling of a button</dd>
    <dt><a href="http://romaricpascal.is/posts/embracing-the-adjacent-sibling-combinator">Embracing the adjacent sibling combinator</a></dt>
    <dd>An article in praise of the adjacent sibling combinator for spacing elements</dd>
    <dt><a href="https://gitlab.com/romaricpascal/markup-engineering-tools">Markup engineering tools</a></dt>
    <dd>An outline (I'll hopefully flesh out some more) organising concepts for styling markup</dd>
</dl>

<div class="tech-list prose-block">

More on <a href="https://romaricpascal.is/posts/">my blog</a>

</div>

</article>

</section>

<section class="no-break--inside">

Skills
---

<article class="project">

### CSS

- writing CSS from scratch and/or integrate with libraries (Bootstrap or Tailwind)
- pick when to use utilities, component specific classes or more generic abstractions
- nudge towards more semantic markup throuh the choice of selectors

### Semantics and accessibility

- write semantic HTML, enhanced with extra ARIA attributes when necessary
- general understanding of WCAG
- screen-reader testing (mainly VoiceOver. though)

### Integrate with a variety of component frameworks

- client side JavaScript: Vue, Svelte, (P)React
- server side: Rails/Django templates, View Components

### Teamwork

- document code and architecture, either inline or as separate documents
- try to provide sensible abstraction and defaults
- eagerness to discuss solutions to problems and collaborate with backend, design and the rest of the team
- promote best practices through pairing, code reviews and sharing of articles

### Languages

- English: Fluent
- French: Mother tongue
- Italian: Conversational

</article>

<section>

<section class="no-break--inside">

Outside
---

When not looking after code, you'll likely find me drawing or painting, playing video or board games (Oath, Root, Sub Terra or Railroad Ink especially), or running (sometimes after a frisbee).

</section>
