---
title: About me
pageTitle: More about me
noForHireCTA: true
---

<div class="project no-heading-counter">

## Long story short

[A condensed version](/me/) is available, but if you're looking for details, please read away!

</div>

<div>

<img src="/media/me.png" alt="" class="avatar no-border" role="presentation">

**Hello, I'm Romaric** and I've been building websites for more that 14 years now. Along the way, I've come to care about accessibility and performance to offer a good experience to the widest audience.

I focus on writing semantic templates and components, as well as maintainable CSS (from scratch or on top of libraries like Bootstrap), in support of various backends whether server-side like Rails or Django, or client-side like React or Vue.

</div>

I'm attached to the privacy of data, but a big believer in the sharing of knowledge.
This is why I try to keep the [code of my personal projects][source-repositories] open and [share what I learnt](#writings).

When not looking after code, you'll likely find me drawing or painting, playing board or video games, or running (sometimes after a frisbee).

Currently: Amba Health and Care - Front-end lead
---

Since [CookiesHQ]'s acquisition by [Amba], I'm carrying on the responsibilities of my previous role, only on a unique project facilitating the care for elderlies rather than a variety of applications:

- conceiving and implementing front-end components, in coordination with backend developers, product owners and the rest of the team more generally
- team support on front-end issues through pairing or code review
- introducing component architecture, using [View Component], to structure our front-end code
- advocating for accessibility in the features we develop and keeping an eye for it at QA time
- promoting best practices and interesting patterns around front-end through code reviews and the sharing of technical articles

<article class="project">

### Technologies

Rails, View Component, HTML (HAML), CSS (Bootstrap, SCSS), JavaScript (Turbo, Stimulus, Lit), tooling (Webpack)

</article>

I'm also trying to open-source non-sensitive pieces of code, mostly in relation to the use of components. Hopefully I'll be able to release them as proper Ruby gems as soon as I can, but in the meantime, you can find their repositories:

<article class="project project--side">

### [merge_attributes]

A Rails helper to merge hashes to be used as attributes for HTML elements, with special handling of `class` or other, configurable, attributes (like Stimulus `data-controller` or `data-action`, or ARIA's `aria-labelledby`).

</article>

<article class="project project--side">

### [View Component concerns][view-component_concerns]

Reflexion and helper modules around recurring patterns when using [View Component: having a unique root element, computing its attributes, computing IDs/class names...

</article>

Previously
---

### Nov. 2017 - Jan 2022: [CookiesHQ] - Front-end developer/lead

<p class="no-margin-top">Excluding <a href="#mar-2020---sep-2020-personal-explorations">Mar. - Sep. 2020</a></p>

Conception and development of front-end components for various applications, coordinating with the backend developers and designers. Alongside development, I also helped support the team when front-end issues arose. I also raised awareness about accessibility through:

- [a webinar (video with subtitles, automatic ones though 😔)][smart-cookies-webinar]
- job specific (designer, developer, QA) internal guides to decrypt WCAG rules 
- [a public presentation][smart-cookies-presentation] at the [Smart Cookies meetup][smart-cookies-accessibility]
- [articles on the company's blog][articles-at-cookies]
- [an internal presentation][cookieshq-presentation]

<blockquote>

He was a key member of our team, leading the front-end development on many projects. He was our accessibility and ethics champion. He has challenged us more than anyone else, pushing us to constantly improve our processes and the way we approach projects.

We have learnt a lot from him. I think that, going forward, Nic and I will always have a little Romaric's voice in our heads but above all, we're very grateful for the 2 years that he has spent with us.

<cite><a href="https://www.linkedin.com/feed/update/urn%3Ali%3Aactivity%3A6638408058257055744/">Nathalie Alpi - Co Founder and Managing Director at CookiesHQ</a></cite>
</blockquote>

<article class="project">

#### [Caption.Ed][caption.ed]

<p class="lead">
Component system to structure the UI and its future development
</p>

To provide structure the software's UI and help abstract the complexity of some front-end patterns, I created a set of components relying on [View Component] and [Tailwind]. From generic UI component (link/buttons, forms, data tables...) to "business" ones rendering information specific to the application, they presented a mix of levels of abstraction:
- markup: ad-hoc HTML and/or composition with other components
- CSS: app-wide classes or component specific ones
- JavaScript: light JavaScript enhancement with [Stimulus] to full-JS component with Preact

<p class="tech-list">Rails, View Component, HTML (ERB), CSS (Tailwind, BEM), JavaScript (Turbo, Stimulus, Preact), tooling (Webpack)</p>


</article>

<article class="project">

#### [Stornaway][stornaway]

<p class="lead">
Development of a graph editor to facilitate the creation of interactive video.
</p>

Aside from general styling, I mostly got involved with the implementation of the graph editor. It allows creators to map the stories they're building: which scenes make part of the story, which options viewers can take at the end of the scenes and how that affects the scene they're led to. They can then upload the video for each scene and export their story to play in the app's interactive video player.

This included:

- Prototyping for feasibility the extra features to be added to the graph library (JointJS/RappidJS)
- Collaborating with the back-end developers on the architecture for storing and loading the graph
- Integrating the graph edition with Rails forms for editing the node's data
- Implementing specialised form controls (tabs, file upload, colour picking)
- Updates to the existing code for the interactive player
- Developing a web extension to automate integration with YouTube Studio

<p class="tech-list">Rails, HTML (HAML), CSS (Bootstrap, SASS, SCSS), JavaScript (RappidJS), tooling (Webpack)</p>

</article>

<article class="project">

#### [Big Clean Switch][big-clean-switch]

<p class="lead">
Development of a "block editor" to help create variations of a green energy supplier switching form
</p>

To help Big Clean Switch customize the journey their users go through when looking for a new green energy provider, I helped:

- implement a "block editor" that lets them organise pre-set fields into different steps and pick a theme of colours matching their commercial partner,
- export the resulting form to integrate on their Wordpress site or 3rd party, ensuring minimal bloat (though use of vanilla JS and bespoke CSS) and preventing styles leaking in.

This involved:

- Semantic implementation of various form control components
- Responsive styling of the form with CSS
- Building JavaScript navigation through the step and form fields behaviour (error handling, conditional visibility, adapting values to choices...)
- Offering styling options (theme switching)
- Collaborating to the architecture for handling form's saves, rendering and submission
- Allowing embedding of the form on the marketing site, as well as 3rd parties

<p class="tech-list">Rails, HTML (HAML), CSS (SASS, PostCSS), JavaScript (Vanilla), tooling (Webpack)</p>

</article>

<article class="project">

#### [Patternbank][patternbank]

<p class="lead">
Development of a block editor to facilitate the creation of content pages to alongside ecommerce site.
</p>

To help Patternbank publish content next to their ecommerce site, I helped implement a "block editor" that allows to build pages through a combination of configurable sections. Due to high importance of visuals for this site, particular attention had to be paid to:

- image responsiveness and performance (lazy loading, srcset, responsive backgrounds)
- providing a variety of responsive layouts for each section
- offering styling options (typography, colours, component variants)
- allowing accessible fluid typography
- providing control for heading levels

On a more technical level, this project included:

- Making a client-side only prototype with Vue to quickly explore the concept
- Collaborating, both on design and implementation, with the backend developers for storing and loading the content
- Implementing components and the page rendering
- Styling the components with CSS
- Integrating a Vue editor to pick sections and configure their options with Rails form in the admin

<p class="tech-list">Rails, HTML (HAML), CSS (SASS, PostCSS), JavaScript (Vue, Vanilla), tooling (Webpack)</p>

</article>

<article class="project project--not-linked">

#### Update existing ecommerce sites

<p class="lead">Implement design updates and new features on ecommerce sites already in production</p>

On ecommerce sites like [Good Sixty], [Patternbank][patternbank-studio] or [Roughtrade], I implemented updates to the user interface especially:

- product listing
- filtering forms
- product pages
- checkout

<p class="tech-list">Rails, HTML, CSS (SASS), JavaScript (jQuery, Turbolinks)</p>

</article>

### Mar. 2020 - Sep 2020: Personal explorations

I [left CookiesHQ for a while](#nov-2017---jan-2022-cookieshq---front-end-developerlead) to do some personal explorations. I developped [some experimental projects](#other-projects) as well as more complete pieces of work.

<article class="project">

### [Personal site][romaricpascal.is]

<p class="lead">Explore static site generation, internationalisation and steady writing.</p>

Starting back from scratch, I rebuilt this website using a static site generator, implementing features to publish both in English and French. The whole process is documented through [blog posts](/posts/), from the JavaScript implementing the features to the CSS patterns used for the styles, as well as accessibility fundamentals.

<p class="tech-list">JavaScript (NodeJS), Metalsmith, HTML, CSS, Markdown</p>

</article>

<article class="project">

### [To/Fro][to-fro]

<p class="lead">Volunteer collaboration to an app that coordinates <a href="https://knowlewestalliance.co.uk/">Knowle West Alliance</a> volunteers who support isolated people during the Covid-19 crisis.</p>

I took part to the [EUvsVirus](https://www.euvsvirus.org/) hackathon to kickstart the project and continued growing the project afterwards. Due to the small scale of the team, I got involved in most areas of the build: setting up the front-end build process, implementing Django templates and their styling using Bootstrap, but also using Django's ORM to query the necessary data or customizing Django admin, authentication and emails.

<p class="tech-list">Django, HTML, CSS (Bootstrap, SASS), JavaScript (jQuery), tooling (Parcel)</p>

</article>

### Before Nov. 2017

- Jan. 2016 - Nov. 2017: Independent lettering artist
- Jan. 2012 - Dec. 2016: Independent front-end developer and web designer - Works included building a JavaScript UI for [HP Labs' Loom][hpe-loom].
- Apr. 2008 - Aug. 2011: Atos Wordline - Intern to Lead developer on the <span lang="fr">Géoportail</span> mapping API for the French <span lang="fr">Institut géographique national</span>.

Other projects
---

Outside of client work, and [this website][website-sources], I also tinker with various little projects: small apps, tooling or just experiments with concepts or technology that feel interesting to me. Here are a selected few:

<article class="project project--side">

### [Harvrest]

A [Svelte pomodorro app (sources on Gitlab)][harvrest-sources] where you accrue more rest if your working session goes over 25min.

</article>

<article class="project project--side">

### [express-mount-files]

An ExpressJS middleware to declare routes through folders and files, nostalgia of PHP's early days.

</article>

<article class="project project--side">

### [implicitjs]

Twisting JavaScript syntax with Babel to try and turn it into a templating language. Highly experimental.

</article>

<article class="project project--side">

### [An experiment with Bezier curves][bezier-experiment]

A Svelte app illustrating an idea for drawing a Bezier spline passing through a list of points

</article>

<article class="project project--side">

### [Taskfight]

[A(n oldish) React app (sources on Gitlab)][taskfight-sources] where tasks fight against one another to sort their priorities

</article>

Writings
---

I learnt a lot from what others shared, so it's only normal to try and reciprocate (with varying success at regularity):

- [On this site](/posts/): Currently sharing a step by step of the site's rebuild
- [On CookiesHQ blog][articles-at-cookies]: Accessibility and performance tips from when I was working there
- [On my previous site](https://old.romaricpascal.is/writing-about/): Mix of lettering and web development articles

[stornaway]: https://www.stornaway.io/#homevideoplayer
[big-clean-switch]: https://bigcleanswitch.org/
[patternbank]: https://patterbank.com
[patternbank-studio]: https://patternbank.com/studio
[Roughtrade]: https://www.roughtrade.com/gb/s
[Good Sixty]: https://www.goodsixty.co.uk/retailers/bristol/115-earthcake
[to-fro]: https://github.com/cgillions/to-fro/
[hpe-loom]: https://community.hpe.com/t5/behind-the-scenes-at-labs/introducing-loom-a-new-tool-for-managing-complex-systems/ba-p/6793962
[hpe-loom-source]: https://github.com/HewlettPackard/loom
[cookieshq-presentation]: https://www.cookieshq.co.uk/posts/building-accessible-websites-is-a-job-for-the-whole-team
[articles-at-cookies]: https://www.cookieshq.co.uk/posts/author/romaric
[smart-cookies-presentation]: https://cookieshq.github.io/accessibility-smart-cookies
[smart-cookies-accessibility]: https://www.meetup.com/Smart-Cookies-Bristol/events/264888812/
[smart-cookies-webinar]: https://www.youtube.com/watch?v=cv0o-2EH28Q
[Taskfight]: https://taskfight.romaricpascal.com
[express-mount-files]: https://gitlab.com/romaricpascal/express-mount-files
[implicitjs]: https://gitlab.com/romaricpascal/implicitjs
[romaricpascal.is]: https://gitlab.com/romaricpascal/romaricpascal-is
[bezier-experiment]: https://gitlab.com/romaricpascal/bezier-spline-experiment
[source-repositories]: https://gitlab.com/romaricpascal/
[website-sources]: https://gitlab.com/romaricpascal/romaricpascal-is/
[CookiesHQ]: https://cookieshq.co.uk
[Harvrest]: https://harvrest.com
[harvrest-sources]: https://gitlab.com/romaricpascal/harvrest/
[taskfight-sources]: https://gitlab.com/romaricpascal/taskfight
[caption.ed]: https://captioned.talk-type.com/
[View Component]: https://viewcomponent.org
[Tailwind]: https://tailwindcss.com/
[Stimulus]: https://stimulus.hotwired.dev/
[amba]: https://amba.co
[merge_attributes]: https://github.com/Amba-Health/merge_attributes
[view-component_concerns]: https://github.com/Amba-Health/view_component-concerns

