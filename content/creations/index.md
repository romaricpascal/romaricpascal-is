---
title: Creations
---
Just a place to keep track of my arts or craft creations, as I'm slowly getting momentum back after a long break [since I stopped doing lettering](https://old.romaricpascal.is), and remind me of the journey.
