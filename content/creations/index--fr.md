---
title: "Créations"
---
Juste un petit coin où j'amasse mes creations artistiques, maintenant que j'y retrouve le goût depuis [mes travaux de lettrage (en)](https://old.romaricpascal.is), pour se rappeler du chemin parcouru.
