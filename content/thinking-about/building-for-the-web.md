---
title: Building for the web
date: 2022-02-28
draft: true
---

Web pages are built on 3 elements:

- markup providing the structure of the content, HTML possibly mixed with some SVG for some graphic elements
- styles controlling their rendering: layout, size, typography, cosmetic, that's CSS
- scripts to update the page locally in response to user interactions (among other events), JavaScript

They can also load various kinds of media (images, fonts, audio, video, PDFs), but we'll leave that aside.

Users can load these in a variety of browsers, and voila, they see a web page. Of course, it's not that simple on the developer side, but the web platform really comes to help:

- the 3 elements discussed earlier are well specified, a foundation to help different browsers get a consistent render
- browsers doing a better and better job at implementing these specifications, limiting the differences from one to another

It's still not perfect, not sure if it'll ever be (or ever should), but it's definitely better than ever. And it's not just a matter of making old things consistent, browsers are constantly bringing in new features that we can use as developers, be it:

- better support for the markup semantics, that they'll pass down through the operating system so that not only screens can display the information, but assistive technologies like screen readers get it too
- more and more poweful styling capacities, from new layout features, custom properties or upcoming ways to better separate the responsibilities in the code...
- support for modern JavaScript syntaxes as well as native APIs

This page is kind of a bookmark for the different places to look for information about either:

- the specifications
- the browsers

The platform provides
---

Specifications are nice, but not necessarily what you want to read while building your pages.
MDN provides a much nicer source of information, with not only references but also tutorials.
If your network is shaky, https://devdocs.io/ can get you some offline access (and not just to the platform refs, but popular libraries as well.)

### Markup

- Specs: 
  - HTML: https://html.spec.whatwg.org/dev/
  - SVG https://svgwg.org/svg2-draft/Overview.html
- MDN: https://developer.mozilla.org/en-US/docs/Web/HTML/Element
- Compatibility: https://caniuse.com

### CSS

- CSS: https://www.w3.org/Style/CSS/current-work
- MDN: https://developer.mozilla.org/en-US/docs/Web/CSS
- Compatibility: https://caniuse.com/
- Methodologies: [BEM for naming](https://getbem.com), [ITCSS](https://www.creativebloq.com/web-design/manage-large-css-projects-itcss-101517528) for organisation
- Also useful: https://web.dev/learn/css/, https://smolcss.dev/, https://every-layout.dev/

### JavaScript

Specs:
- EcmaScript - TC39: https://tc39.es/ecma262/. Especially useful API: Array, XHR/fetch, Promise 
- DOM & other API: https://www.w3.org/TR/?tag=dom Notably, each element has their DOM API: under `HTML<NameOfTheElement>Element`. for ex. `<select>` under `HTMLSelectElement`. Same goes for SVG, expect it's `SVG` instead of `HTML`, like in `SVGPathElement` for `<path>`. On a day to day basis, it's also good to be aware of event propagation: https://domevents.dev/.
- MDN is, as usual, a strong resource: https://developer.mozilla.org/en-US/docs/Web/API/HTML_DOM_API
- Compatibility: https://kangax.github.io/compat-table/es6/
- Also useful: https://javascript.info, https://exploringjs.com/, https://1loc.dev/

### Accessibility

Starts with strong HTML markup, but advanced patterns may need:
- extra semantics brought by ARIA
- proper use of CSS to ensure correct visibility of the content to the appropriate media
- JavaScript to provide reaction to the appropriate inputs and focus management for complexe interactions

- WCAG: https://www.w3.org/WAI/WCAG21/quickref/
- ARIA Spec: https://www.w3.org/TR/wai-aria-1.2/
- ARIA authoring practice: https://www.w3.org/TR/wai-aria-practices-1.2/
- Compatibility: https://a11ysupport.io/, https://www.powermapper.com/tests/screen-readers/
- WebAIM, especially their community: https://webaim.org/community/
- Also useful: https://www.scottohara.me/, https://sarahmhigley.com/, https://adrianroselli.com/, Gov design systems & docs (https://design-system.service.gov.uk/, https://accessibility.18f.gov/), 

The browsers do too
---

### Browsing the web

What matters is actually the rendering engine, of which there are 3 main ones:

- Blink + V8 (Chrom(e/ium), Edge, Vivaldi): https://bugs.chromium.org
- Gecko + SpiderMonkey (Firefox): https://bugzilla.mozilla.org/
- Webkit (Safari/Epiphany): https://bugs.webkit.org/

Browser changelogs:

Firefox: https://www.mozilla.org/en-US/firefox/releases/
Chrome: https://chromereleases.googleblog.com/
Edge: https://docs.microsoft.com/en-us/deployedge/microsoft-edge-relnote-stable-channel
Safari: https://developer.apple.com/documentation/safari-release-notes

### Screenreaders

Some users complement the browser with a screenreader. (To be checked: Actually, it's not really their browser they complement with a screen-reader but their operating system. Browser pass the info to the OS, which is then relayed to the screen-reader).

- VoiceOver on Mac/iOS
- JAWS/NVDA/Narrator on Windows
- Orca on Linux
- ChromeVox on ChromeOS
- Talkback on Android

More details on screen-reader/browser combinations: https://webaim.org/projects/screenreadersurvey9/#browsercombos
For how to use them: https://dequeuniversity.com/screenreaders/

### The devtools

Desktop browsers don't just allow us to browse the web. They also provide powerful development tools (each in their own flavour).
On a day to day basis:

- Elements: provide the markup currently rendered on the page (for ex. after some JavaScript altered it since it was first downloade), allows to highlight what corresponds to a specific tag (and the other-way around), as well as inspect and edit the styles applied to it, both the different ruleset that apply to it and which value the browser computed for each property applied to the element.
- Network: helps monitor requests, both those made by the browser (eg. loading CSS, JS, images...) and those made from JavaScript code (eg. XHR/fetch requests). It only tracks the requests that happen while the devtools are open, so you'll likely need to reload your page to see what you're after. Also allows to disable caching (I set it on when developing), simulate bad network conditions or even completely block some requests.
- Console: That's where any `console.log` (or other method) end up, as well as whatever error/warning the browser encounters while rendering the page. It's not just for reading, it allows to run arbitrary piece of code, which can be very helpful for quickly trying stuff out. `$0` is a shortcut to the selected elemet in the elements panel. It has its own panel, but can be displayed next to the other pannels (press `Escape`)
- Storage: Provides access to all the forms of storage the page can handle: cookies, session and localStorage, IndexDB
- Sources/(Debug + Style editor): Inspect the sources of the CSS/JavaScript loaded on the page, add break/logpoints to debug JavaScript execution

When developing for mobile, they also emulate a mobile browser (viewport size, touch interactions), but you can also make them connect to an actual mobile browser to test in a more real situation.
