---
title: Board games
---

Current library of board games, in no particular order (for now...)

## Two and more players

1. [Skora](https://boardgamegeek.com/boardgame/271645/skora)
1. [Here to slay](https://boardgamegeek.com/boardgame/299252/here-slay)
1. [Welcome to](https://boardgamegeek.com/boardgame/233867/welcome)
1. [Root](https://boardgamegeek.com/boardgame/237182/root)
1. [Kingdomino origins](https://boardgamegeek.com/boardgame/340041/kingdomino-origins)
1. [7 Wonders](https://boardgamegeek.com/boardgame/68448/7-wonders)
1. [Dead of winter](https://boardgamegeek.com/boardgame/150376/dead-winter-crossroads-game)
1. [Ticket to ride Europe](https://boardgamegeek.com/boardgame/14996/ticket-ride-europe) + pink trains
1. [Deep sea adventure](https://boardgamegeek.com/boardgame/169654/deep-sea-adventure)
1. [Carcassone](https://boardgamegeek.com/boardgame/822/carcassonne) + [Inns & Cathedrals expansion](https://boardgamegeek.com/boardgame/2993/carcassonne-expansion-1-inns-cathedrals)
1. [Sushi Go Party](https://boardgamegeek.com/boardgame/192291/sushi-go-party)
1. [Oath](https://boardgamegeek.com/boardgame/291572/oath-chronicles-empire-and-exile)
1. [Kanagawa](https://boardgamegeek.com/boardgame/200147/kanagawa)
1. [Azul](https://boardgamegeek.com/boardgame/230802/azul)
1. [Tokyo Highway](https://boardgamegeek.com/boardgame/215463/tokyo-highway)
1. [New York Zoo](https://boardgamegeek.com/boardgame/300877/new-york-zoo)
1. [RailRoad Ink (Lush Green)](https://boardgamegeek.com/boardgame/306881/railroad-ink-challenge-lush-green-edition)
1. [Tiny Epic Galaxy Blast off](https://boardgamegeek.com/boardgame/317105/tiny-epic-galaxies-blast)
1. [Set](https://boardgamegeek.com/boardgame/1198/set)
1. [Kodama](https://boardgamegeek.com/boardgame/181810/kodama-tree-spirits)
1. [The Builders - Middle ages](https://boardgamegeek.com/boardgame/144553/builders-middle-ages)
1. [Rise of Augustus](https://boardgamegeek.com/boardgame/137297/rise-augustus)
1. [Monopoly Deal](https://boardgamegeek.com/boardgame/40398/monopoly-deal-card-game)
1. [Citadels](https://boardgamegeek.com/boardgame/478/citadels)
1. [Hidden Leaders](https://boardgamegeek.com/boardgame/320718/hidden-leaders)
1. [Tiwanaku](https://boardgamegeek.com/boardgame/267979/tiwanaku)
1. [Tinderblox](https://boardgamegeek.com/boardgame/295192/tinderblox)
1. [ICE](https://boardgamegeek.com/boardgame/306482/i-c-e)

## Two players

1. [Soulaween](https://boardgamegeek.com/boardgame/269766/soulaween) + [extension](https://boardgamegeek.com/boardgame/302305/soulaween-little-girl)
1. [Crossing](https://boardgamegeek.com/boardgame/172971/crossing)
1. [Takenoko](https://boardgamegeek.com/boardgame/70919/takenoko) + [extension](https://boardgamegeek.com/boardgame/174095/takenoko-chibis)
1. [7 Wonders duel](https://boardgamegeek.com/boardgame/173346/7-wonders-duel)
1. [Jaipur](https://boardgamegeek.com/boardgame/54043/jaipur)
1. [Control](https://boardgamegeek.com/boardgame/192240/control)
1. [Mythic Mischief](https://boardgamegeek.com/boardgame/342894/mythic-mischief)
1. [Star Realms](https://boardgamegeek.com/boardgame/147020/star-realms) + [Star Realms: Colony Wars](https://boardgamegeek.com/boardgame/182631/star-realms-colony-wars)

## Co-operative

1. [Magic Maze](https://boardgamegeek.com/boardgame/209778/magic-maze)
1. [Sub Terra](https://boardgamegeek.com/boardgame/204472/sub-terra)
1. [Pandemic](https://boardgamegeek.com/boardgame/30549/pandemic)
1. [Tranquility](https://boardgamegeek.com/boardgame/288513/tranquility)
1. [The mind](https://boardgamegeek.com/boardgame/244992/mind)
1. [Hanabi](https://boardgamegeek.com/boardgame/98778/hanabi/credits#alternatename)
1. [Les taxis de la marne](https://boardgamegeek.com/boardgame/181005/les-taxis-de-la-marne)

## Play once

1. [TIME. Stories](https://boardgamegeek.com/boardgame/146508/time-stories) + [White Cycle extensions](https://boardgamegeek.com/boardgamefamily/55922/game-time-stories-white-cycle)
1. [Unlock - Les secrets de la pieuvre (in French)](https://boardgamegeek.com/boardgame/368035/unlock-short-adventures-secrets-octopus)
1. [Curse of the dark](https://boardgamegeek.com/boardgame/361923/curse-dark)
1. [Escape Tales - Low Memory](https://boardgamegeek.com/boardgame/282291/escape-tales-low-memory)
1. [The Mystery Agency - The Balthazar Stone](https://themysteryagency.com/product/the-balthazar-stone/)

### Already played
1. [Unlock - Mystery adentures](https://boardgamegeek.com/boardgame/228867/unlock-mystery-adventures)
1. [Unlock - Epic adventures](https://boardgamegeek.com/boardgame/294612/unlock-epic-adventures)
1. [Unlock - Timeless adventures](https://boardgamegeek.com/boardgame/279613/unlock-timeless-adventures)
1. [Exit - The Pharaoh's Tomb](https://boardgamegeek.com/boardgame/203416/exit-game-pharaohs-tomb)
1. [Exit - Dead Man on the Orient Express](https://boardgamegeek.com/boardgame/226522/exit-game-dead-man-orient-express)
1.  [Exit - The Forbidden Castle](https://boardgamegeek.com/boardgame/215840/exit-game-forbidden-castle)
10. [TIME Stories Revolution: The Hadal Project](https://boardgamegeek.com/boardgame/287742/time-stories-revolution-hadal-project)
11. [TIME Stories Revolution: A Midsummer Night](https://boardgamegeek.com/boardgame/271524/time-stories-revolution-midsummer-night)

## Trading Card Games

1. [Keyforge](https://boardgamegeek.com/boardgame/257501/keyforge-call-archons) decks (4)
1. Magic the gathering decks (2 [Kamigawa](https://boardgamegeek.com/boardgame/240310/magic-gathering-champions-kamigawa) decks + bundles for [Wilds of Eldraine](https://boardgamegeek.com/boardgame/400716/magic-gathering-wilds-eldraine) + [Strixhaven](https://boardgamegeek.com/boardgame/359415/magic-gathering-strixhaven-school-mages) + [Kaldheim](https://boardgamegeek.com/boardgame/360733/magic-gathering-kaldheim))

