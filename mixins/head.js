export default {
	head() {
		const { title, description } = this.headData

		const head = { meta: [] }
		if (title) {
			Object.assign(head, {
				// Will be formated using the `titleTemplate`
				title,
				// But the Open Graph meta doesn't need the formatting
				// so needs a separate value
				meta: [
					{
						hid: 'ogTitle',
						name: 'og:title',
						content: title,
					},
				],
			})
		}

		if (description) {
			head.meta.push({
				hid: 'description',
				name: 'description',
				content: description,
			})
		}

		return head
	},
}
